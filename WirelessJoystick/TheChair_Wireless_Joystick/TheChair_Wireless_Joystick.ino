#include <Debounce.h>
#include <Arduino.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define DEBUG
/*JOYSTICK Definitions*/
#define L_TRIG 6        // Pin used for left trigger
#define R_TRIG 3        // Pin used for right trigger
#define UP_BTN 8        // Pin used for right trigger
#define R_BTN 9        // Pin used for right trigger
#define L_BTN 4        // Pin used for right trigger
#define DN_BTN 2        // Pin used for right trigger
#define JOYPRESS_BTN 5        // Pin used for left trigger
#define VERT_JOY_AXIS A3   // Pin used for left joystick
#define HORIZ_JOY_AXIS A2   // Pin used for right joystick

/* DISPLAY DEFINITIONS*/
#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);
#define NUMFLAKES 10
#define XPOS 0
#define YPOS 1
#define DELTAY 2
#define LOGO16_GLCD_HEIGHT 16 
#define LOGO16_GLCD_WIDTH  16 
static unsigned char PROGMEM const logo16_glcd_bmp[] =
{ B00000000, B11000000,
  B00000001, B11000000,
  B00000001, B11000000,
  B00000011, B11100000,
  B11110011, B11100000,
  B11111110, B11111000,
  B01111110, B11111111,
  B00110011, B10011111,
  B00011111, B11111100,
  B00001101, B01110000,
  B00011011, B10100000,
  B00111111, B11100000,
  B00111111, B11110000,
  B01111100, B11110000,
  B01110000, B01110000,
  B00000000, B00110000 };

#if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

/* JOYSTICK VARIABLES*/
char state =0;
bool Joy_Enable=0;
bool DeadManSW=0;
bool USSPreventEnable=0;
bool CruiseEnable=0;
bool SpeedBoost=0;
char joy_X,joy_Y=0;
int set_speed=0;
unsigned long lastStatusTransmitTime = 0;
unsigned long lastXYTransmitTime = 0;
unsigned long transmitStatusInterval = 25;
unsigned long transmitXYInterval = 10;

//create ddebouncer for appropraite pins
Debounce L_TRIG_Debounce = Debounce( 50 , L_TRIG ); 
int L_TRIG_edge,L_TRIG_read=0;
Debounce R_TRIG_Debounce = Debounce( 50 , R_TRIG ); 
int R_TRIG_edge,R_TRIG_read=0;
Debounce UP_BTN_Debounce = Debounce( 50 , UP_BTN ); 
int UP_BTN_edge,UP_BTN_read=0;
Debounce R_BTN_Debounce = Debounce( 50 , R_BTN ); 
int R_BTN_edge,R_BTN_read=0;
Debounce L_BTN_Debounce = Debounce( 50 , L_BTN ); 
int L_BTN_edge,L_BTN_read=0;
Debounce DN_BTN_Debounce = Debounce( 50 , DN_BTN ); 
int DN_BTN_edge,DN_BTN_read=0;
Debounce JOYPRESS_BTN_Debounce = Debounce( 20 , JOYPRESS_BTN ); 
int JOYPRESS_BTN_edge,JOYPRESS_BTN_read=0;

/*SYSTEM STATUS*/
bool recievedInfoFromChair=0;
float temperature;
float temperature_const = 5 / (1024.0*2.6);
float temperature_offset = .2/2.6;
float farenhiet_const = 9/5;
float vin;
unsigned char recvd_char=0;



char assemble_state_transmit()
{
  char transmit_status=0;
  bitWrite(transmit_status, 7, Joy_Enable);
  bitWrite(transmit_status, 6, DeadManSW);
  bitWrite(transmit_status, 5, USSPreventEnable);
  bitWrite(transmit_status, 4, CruiseEnable);
  bitWrite(transmit_status, 3, SpeedBoost);
  if(set_speed == 0)
  {
    bitWrite(transmit_status, 2, 0);
    bitWrite(transmit_status, 1, 0);
    bitWrite(transmit_status, 0, 0);
  }else if(set_speed == 1){
    bitWrite(transmit_status, 2, 0);
    bitWrite(transmit_status, 1, 0);
    bitWrite(transmit_status, 0, 1);
  }else if(set_speed == 2){
    bitWrite(transmit_status, 2, 0);
    bitWrite(transmit_status, 1, 1);
    bitWrite(transmit_status, 0, 0);
  }else if(set_speed == 3){
    bitWrite(transmit_status, 2, 0);
    bitWrite(transmit_status, 1, 1);
    bitWrite(transmit_status, 0, 1);
  }else if(set_speed == 4){
    bitWrite(transmit_status, 2, 1);
    bitWrite(transmit_status, 1, 0);
    bitWrite(transmit_status, 0, 0);
  }else if(set_speed == 5){
    bitWrite(transmit_status, 2, 1);
    bitWrite(transmit_status, 1, 0);
    bitWrite(transmit_status, 0, 1);
  }
  return transmit_status;
}

void setup()
{
  /*SETUP COMMUNICATION PORTS*/
  #ifdef DEBUG
    SerialUSB.begin(9600); // Initialize Serial Monitor USB
  #endif
  Serial1.begin(115200); // Initialize hardware serial port, pins 0/1
 
  pinMode(L_TRIG,INPUT_PULLUP); // Enable pullup resistor for left trigger
  pinMode(R_TRIG,INPUT_PULLUP); // Enable pullup resistor for right trigger
  pinMode(UP_BTN,INPUT_PULLUP); 
  pinMode(R_BTN,INPUT_PULLUP); 
  pinMode(L_BTN,INPUT_PULLUP); 
  pinMode(DN_BTN,INPUT_PULLUP);

  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3D (for the 128x64)
  // init done
  drawChairbot();
  display.display(); // show splashscreen
  delay(2000);
  display.clearDisplay();   // clears the screen and buffer
  
 
  while(!Serial1.available()){//Wait until something is recieved from Chairbot
     drawTurnOnChairbot();
    display.display(); // show splashscreen
    delay(2000);
    }
}

void loop()
{
    /* UPDATE ALL SWITCH DEBOUNCERS*/
    L_TRIG_edge = L_TRIG_Debounce.update( );
    L_TRIG_read = !L_TRIG_Debounce.read();
    R_TRIG_edge = R_TRIG_Debounce.update( );
    R_TRIG_read = !R_TRIG_Debounce.read();
    UP_BTN_edge = UP_BTN_Debounce.update( );
    UP_BTN_read = !UP_BTN_Debounce.read();
    R_BTN_edge = R_BTN_Debounce.update( );
    R_BTN_read = !R_BTN_Debounce.read();
    L_BTN_edge = L_BTN_Debounce.update( );
    L_BTN_read = !L_BTN_Debounce.read();
    DN_BTN_edge = DN_BTN_Debounce.update( );
    DN_BTN_read = !DN_BTN_Debounce.read();
    JOYPRESS_BTN_edge = JOYPRESS_BTN_Debounce.update( );
    JOYPRESS_BTN_read = !JOYPRESS_BTN_Debounce.read();

    /* UPDATE JOYSTICK AXES */
    joy_X=char(analogRead(HORIZ_JOY_AXIS)/4);
    joy_Y=char(analogRead(VERT_JOY_AXIS)/4);
    
    /* UPDATE STATE MACHINE*/
    if(state == 0){
      Joy_Enable=0;
      DeadManSW=0;
      CruiseEnable=0;
      SpeedBoost=0;
      set_speed=0;
      if(L_TRIG_edge && L_TRIG_read)// if there is s rising edge and the value of button is 1
      {
        state = 1; //transition to state 1;
      }
    }else if(state == 1){// manual control on and dead mans switch off
      Joy_Enable=1;
      DeadManSW=0;
      SpeedBoost=0;
      CruiseEnable=0;
      set_speed=0;
      if(L_TRIG_edge && L_TRIG_read)
      {
        state = 0;
      }else if(R_TRIG_read)
      {
        state =2;
      }
    }else if(state == 2){// manual control on and Dead Mans switch On
      Joy_Enable=1;
      DeadManSW=1;
      CruiseEnable=0;
      set_speed=0;
      if(JOYPRESS_BTN_read)
      {
        SpeedBoost=1;
      }else{SpeedBoost=0;}
      if(L_TRIG_edge && L_TRIG_read)
      {
        state = 0;
      }else if(!R_TRIG_read){
        state =1;
      }else if(R_BTN_edge && R_BTN_read)// if cruise contorl btn updated and it is pressed
      {
        state =3;
      }
    }else if(state == 3){// cruise control enabled
      Joy_Enable=1;
      DeadManSW=0;
      CruiseEnable=1;
      SpeedBoost=0;
      if(L_TRIG_edge && L_TRIG_read)
      {
        state = 0;
      }else if(R_BTN_edge && R_BTN_read){// if cruise contorl btn updated and it is pressed
        state = 1;
      }
      else if(R_TRIG_edge && R_TRIG_read){//RB pressed       
        state = 1;
      }else if(UP_BTN_edge && UP_BTN_read){//RB pressed       
          if(set_speed <5)
          {
            set_speed++;
          }
      }else if(DN_BTN_edge && DN_BTN_read){//RB pressed       
          if(set_speed > 0)
          {
            set_speed--;
          }
      }
    }
  
  /* UPDATE USS COLLISION PREVENTION BIT*/
  if(L_BTN_edge && L_BTN_read){
    USSPreventEnable = !USSPreventEnable;}



  /* TRANSMIT Status DATA*/
  if(millis() - lastStatusTransmitTime > transmitStatusInterval)
  {
    lastStatusTransmitTime = millis();
//    SerialUSB.println('S');
//    SerialUSB.println(assemble_state_transmit(),BIN);
    Serial1.write('S');
    Serial1.write(assemble_state_transmit());
    Serial1.write('\r');
    //SerialUSB.println('\r',BIN);
  }
  
  /* TRANSMIT Joystick DATA*/
  if(millis() - lastXYTransmitTime > transmitXYInterval)
  {
    lastXYTransmitTime = millis();
    if((state != 0) && (state != 1))
    {
//      SerialUSB.print('X');
//      SerialUSB.println(joy_X,DEC);
//      SerialUSB.print('Y');
//      SerialUSB.println(joy_Y,DEC);
      Serial1.write('X');
      Serial1.write(joy_X);
      Serial1.write('\r');
      Serial1.write('Y');
      Serial1.write(joy_Y);
      Serial1.write('\r');
   }
  }

  if(Serial1.available())
  {
    recvd_char = Serial1.read();
//    SerialUSB.println(recvd_char,BIN);
      if(recvd_char == 0xff)//need to update display
      {
         drawStatus();
      }else{
          vin = recvd_char << 2;
          vin = vin * temperature_const;
          temperature = (vin - temperature_offset)*135;//celsius
          temperature=temperature*farenhiet_const + 32;
        #ifdef DEBUG
          SerialUSB.println(temperature);
        #endif
      }
  }
 
}

void testdrawchar(void) {
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0,0);

  for (uint8_t i=0; i < 168; i++) {
    if (i == '\n') continue;
    display.write(i);
    if ((i > 0) && (i % 21 == 0))
      display.println();
  }    
  display.display();
}


void drawStatus(void) {
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  
  display.print("Joystick: ");
  if(Joy_Enable){display.println("On");}
  else{display.println("Off");}
  
  display.print("Cruise Control: ");  
  if(CruiseEnable){
    display.println("On");
    display.print("Set Speed: ");
    display.print(set_speed);
    display.println(" MPH");
  }else{display.println("Off");display.println();}
  display.println();
  display.print("PCP Enable: ");
  if(USSPreventEnable){display.println("On");display.println();}
  else{display.println("Off");display.println();}

  display.print("Box Temp.: ");
  display.print(temperature);
  display.print(" F");
  display.display();
}
void drawChairbot(void) {
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.println("   i______i\n   I______I\n   I      I\n   I______I\n  /      /I\n (______( I\n I I    I I\n I      I");
  display.setTextSize(2);
  display.setCursor(65,20);
  display.println("Chair");
  display.setCursor(65,40);
  display.println("Bot");
  
}
void drawTurnOnChairbot(void) {
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.println("   i______i\n   I______I\n   I      I\n   I______I\n  /      /I\n (______( I\n I I    I I\n I      I");
  display.setTextSize(1);
  display.setCursor(65,5);
  display.println(" Turn On");
  display.setTextSize(2);
  display.setCursor(65,20);
  display.println("Chair");
  display.setCursor(65,40);
  display.println("Bot!!");
  
}





