/*
 * ultrasoincs.h
 *
 *  Created on: Mar 31, 2018
 *      Author: mbowyer
 */

#ifndef ULTRASONICS_H_
#define ULTRASONICS_H_

class USS
{
	public:
		USS(int FL_port_in, int FR_port_in, int RL_port_in, int RR_port_in, int Trigger_Port);
		void getUSSDistances();
		int FL_dist, FR_dist, RL_dist, RR_dist;
	private:
		//Distance values for each sensor
//		int FL_dist, FR_dist, RL_dist, RR_dist;

		int FL_port, FR_port, RL_port, RR_port, Trigger_Port;

};



#endif /* SRC_ULTRASONICS_H_ */
