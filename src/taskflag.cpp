/*
 * taskflag.cpp
 *
 *  Created on: Apr 3, 2018
 *      Author: mbowyer
 */

#include "taskflag.h"

TaskFlag::TaskFlag(String taskName_in)
{
	taskName = taskName_in;
	taskFlag = 0;
	taskFlagInUse = 0;
	taskflag_counter=0;
}

void TaskFlag::grab_taskflag()
{
	taskFlagInUse = 1;
}

bool TaskFlag::check_taskflag_in_use()
{
	return taskFlagInUse;
}


bool TaskFlag::reset_taskflag()
{
	taskFlag = 0;
	taskFlagInUse=0;
}

bool TaskFlag::set_taskflag()
{
	taskFlag = 1;
}

bool TaskFlag::check_flag()
{
	return taskFlag;
}
