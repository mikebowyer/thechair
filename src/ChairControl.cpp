/*
 * ChairControl.cpp
 *
 *  Created on: Apr 7, 2018
 *      Author: mbowyer
 */
#include "ChairControl.h"
#include "Arduino.h"

ChairControl::ChairControl(int k)
{
	joyX=0;
	joyY=0;

	state=0;
	Joy_Enable=0;
	DeadManSW=0;
	USSPreventEnable=0;
	CruiseEnable=0;
	set_speed=0;
	lSpeed=0;
	rSpeed=0;
	lastStatusTime=0;
	SpeedBoostEnable=0;
	FL_dist=FR_dist=RL_dist=RR_dist=0;

}

void ChairControl::setJoyStickPos(int channel, unsigned int val)
{
	if(channel == HORZ_AXIS)
	{
		joyX=val;
	}else if(channel == VERT_AXIS ){
		joyY=val;
	}
}


void ChairControl::updateMotorSpeeds()
{
	if(Joy_Enable)
	{
		if(CruiseEnable){
			//TODO
			double temp = (((int) joyX & 0xFF)-128)*ROBOTEQ_SPEED_SCALER/2; // horizontal axis is now between -1000 and 1000
			temp = (temp*set_speed/5);
			if(temp >=0){
				lSpeed = set_speed*200 - temp;
				rSpeed = set_speed*200;
			}else
			{
				lSpeed = set_speed*200;
				rSpeed = set_speed*200+ temp;
			}
		}else if(DeadManSW){
			//convert joydata to range of 128 to -128
			int vert_temp, horiz_temp;
			vert_temp = ((int) joyY & 0xFF)-128;
			horiz_temp = ((int) joyX & 0xFF)-128;

			//calculate speeds of motors based on Skid steer control and
			//using Roboteq Speed scaler which takes a 256 range value to a 1024 range
			lSpeed=(vert_temp-(horiz_temp*TURN_DAMPER))*ROBOTEQ_SPEED_SCALER/2;
			rSpeed=(vert_temp+(horiz_temp*TURN_DAMPER))*ROBOTEQ_SPEED_SCALER/2;

			if(SpeedBoostEnable){
				rSpeed=rSpeed*2;
				lSpeed=lSpeed*2;
			}

		}else{
			lSpeed=0;
			rSpeed=0;
		}
	}else{
		lSpeed=0;
		rSpeed=0;
	}
	if(USSPreventEnable)
			{
	//			Serial.print("\n\nLeft Speed before: ");
	//			Serial.println(lSpeed);
	//			Serial.print("Right Speed before: ");
	//			Serial.println(rSpeed);
				if( (rSpeed < 0) || (lSpeed < 0) ) // going backwards...
				{
					if(RL_dist < USSPREVSTOPDIST){
						rSpeed = 0;
						lSpeed = 0;
					}
					else if(RL_dist < USSSLOWDIST)//something is close
					{
						double temp = (double)RL_dist/USSSLOWDIST;
						rSpeed = rSpeed * temp;
						lSpeed = lSpeed * temp;
					}
				}
//				if(rSpeed > lSpeed)//turning left
//				{
//					if(FL_dist < USSPREVSTOPDIST){
//						rSpeed = 0;
//					}
//					else if(FL_dist < USSSLOWDIST)//something is close
//					{
//						double temp = (double)FL_dist/USSSLOWDIST;
//						rSpeed = rSpeed * temp;
//					}
//				}else if (lSpeed > rSpeed){
//					if(FR_dist < USSPREVSTOPDIST){
//						lSpeed = 0;
//					}
//					else if(FR_dist < USSSLOWDIST)//something is close
//					{
//						double temp = (double)FR_dist/USSSLOWDIST;
//						lSpeed = lSpeed * temp;
//					}
//				}
	//			Serial.print("Left Speed After: ");
	//			Serial.println(lSpeed);
	//			Serial.print("Right Speed After: ");
	//			Serial.println(rSpeed);
	}
	//ensure speed for left motor is not outside range and is not in deadzone
	if(lSpeed>1000){lSpeed=1000;}
	else if (lSpeed < -1000){lSpeed=-1000;}
	else if (abs(lSpeed)< DEADZONE_LIMIT){lSpeed=0;}

	//ensure speed for right motor is not outside range and is not in deadzone
	if(rSpeed>1000){rSpeed=1000;}
	else if (rSpeed < -1000){rSpeed=-1000;}
	else if (abs(rSpeed)< DEADZONE_LIMIT){rSpeed=0;}
#ifdef FLIP_MOTOR_DIR
	rSpeed= -rSpeed;
	lSpeed= -lSpeed;
#endif

}

signed int ChairControl::getLeftSpeed()
{
	return lSpeed;
}

signed int ChairControl::getRightSpeed()
{
	return rSpeed;
}

void ChairControl::updateStatus(unsigned char payload)
{
	Joy_Enable=bitRead(payload,7);
	DeadManSW=bitRead(payload,6);
	USSPreventEnable=bitRead(payload,5);
	CruiseEnable=bitRead(payload,4);
	SpeedBoostEnable=bitRead(payload,3);
	set_speed= payload & 0x07;
	lastStatusTime = millis();
}

unsigned long ChairControl::getLastStatusTime(){
	return lastStatusTime;
}
void ChairControl::updateUSSDistances(int FL_dist_in,int FR_dist_in,int RL_dist_in,int RR_dist_in )
{
	FL_dist=FL_dist_in;
	FR_dist=FR_dist_in;
	RL_dist=RL_dist_in;
	RR_dist=RR_dist_in;

}
