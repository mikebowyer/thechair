/*
 * taskflag.h
 *
 *  Created on: Apr 3, 2018
 *      Author: mbowyer
 */

#ifndef SRC_TASKFLAG_H_
#define SRC_TASKFLAG_H_

#include "Arduino.h"

class TaskFlag
{
	public:

	/**
	 * Creates a task flag for interrupt to set, and semaphore for main process to grab.
	 * @param task_name is the name of the task for which the flag is used for.
	 */
		TaskFlag(String task_name);
		/**
		 * allows the main process to grab the task flags semaphore
		 */
		void grab_taskflag();
		/**
		 * Allows an interrupt to check the semaphore first, without touching the task flag
		 * @return 1 if in use, 0 if not in use
		 */
		bool check_taskflag_in_use();
		/**
		 * if semaphore is not locked, then flag can be set by interrupt.
		 * @return
		 */
		bool set_taskflag();
		/**
		 * When task is done, the flag can be reset.
		 * @return 1 if reset sucessful
		 */
		bool reset_taskflag();
		/**
		 * Check if the flag is set
		 * @return 1 if flag is set, 0 otherwise
		 */
		bool check_flag();
		int taskflag_counter;
	private:
		/**
		 * Flag is set by timer interrupt to inform main loop that this task needs to be completed
		 */
		bool taskFlag;
		/**
		 * Semaphore for the task flag
		 */
		bool taskFlagInUse;
		/**
		 * name of the task for which this is being used
		 */
		String taskName;


};



#endif /* SRC_TASKFLAG_H_ */
