/*
 * CHAIRCONTROL.h
 *
 *  Created on: Mar 31, 2018
 *      Author: mbowyer
 */

#ifndef CHAIRCONTROL_H_
#define CHAIRCONTROL_H_

#include "ChairControl.h"
#include "Arduino.h"

#define VERT_AXIS 1
#define HORZ_AXIS 2
#define TURN_DAMPER .3
#define ROBOTEQ_SPEED_SCALER 7.8
#define DEADZONE_LIMIT 75
#define CONTROL_TIMEOUT 500
#define FLIP_MOTOR_DIR 1
#define USSPREVSTOPDIST 400
#define USSSLOWDIST 400

class ChairControl
{
	public:
		ChairControl(int k);
		void setJoyStickPos(int channel, unsigned int val);
		void updateStatus(unsigned char payload);
		void updateMotorSpeeds();
		void updateUSSDistances(int FL_dist_in,int FR_dis_int,int RL_dist_in,int RR_dist_in );

		signed int getLeftSpeed();
		signed int getRightSpeed();

		unsigned long getLastStatusTime();

		unsigned int joyX;
		unsigned int joyY;
		char state;
		bool Joy_Enable;
		bool DeadManSW;
		bool USSPreventEnable;
		bool CruiseEnable;
		bool SpeedBoostEnable;
		unsigned int set_speed;

	private:

		int FL_dist, FR_dist, RL_dist, RR_dist;
		unsigned long lastStatusTime;
		signed int lSpeed,rSpeed;
};



#endif /* SRC_ULTRASONICS_H_ */
