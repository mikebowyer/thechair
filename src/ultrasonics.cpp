/*
 * ultrasonics.cpp
 *
 *  Created on: Mar 31, 2018
 *      Author: mbowyer
 */
#include "ultrasonics.h"
#include "Arduino.h"

USS::USS(int FL_port_in, int FR_port_in, int RL_port_in, int RR_port_in, int Trigger_Port_in)
{
	FL_port = FL_port_in;
	FR_port = FR_port_in;
	RL_port = RL_port_in;
	RR_port = RR_port_in;
	Trigger_Port = Trigger_Port_in;

	FL_dist=FR_dist=RL_dist=RR_dist=0;

	//Start Ultrasonics firing
	pinMode(Trigger_Port_in, OUTPUT);
	digitalWrite(Trigger_Port_in, HIGH);
	digitalWrite( Trigger_Port_in, LOW );

	//put trigger pin into high impedance state
	//pinMode( Trigger_Port_in, OUTPUT );  // now we're sourcing current, i.e. GND
	pinMode( Trigger_Port_in, INPUT ); // now we're tri-stated
}

void USS::getUSSDistances()
{
	FL_dist = 5*analogRead(FL_port);
	FR_dist = 5*analogRead(FR_port);
	RL_dist = 5*analogRead(RL_port);
	RR_dist = 5*analogRead(RR_port);
}

