
## Purpose of TheChair Repository (AKA Chairbot)

The purpose of this repository is to store the source code the for robot known as Chairbot which is part of the Intelligent Systems Club. 
The tear down and build back up of Chair bot was done for credit as part of ECE554 Course at the University of Michigan-Dearborn. The person responsible for the recreation of Chairbot is Michael Bowyer.
![Scheme](images/ChairbotJoystick.jpg =200x)
---
## Overview of Chair Bot
The following image shows all of the main components and sensors internal to Chair bot which are used for it's functions. There are two main microcotrollers which have both been programmed for this robot. They are the main arduino Mega2560 in the robot itself, and the external wireless joystick. 
![](images/ChairBot_Data_Connections.png =500x)

---
## Operation of Chair bot
Chairbot implements a few functions. They include:

1. Manual Operation: User can drive Chairbot manually by enabling the joystick then holding the dead mans switch.

2. Cruise Control: User can set speed up to 5 MPH by enabling joystick, holding right bumper and pressing right button. Speed can be increased and decreased with up and down buttons.

3. Parking Collision Prevent: User can be aided in parking manuevers when this functino is enabled. It will slow the vehicle down when Chairbot sense a nearby object and stop if it is too close.

A video of how to use the controller to enable/disable the various settings can be found at the following video:
https://youtu.be/Bt8HygxV-Y8

---

## Required Libraries
**Required Libraries for Wireless Joystick**
The wireless joystick requires the following libraries: 
1. Debounce Library: https://github.com/wkoch/Debounce
2. Adafruit_GFX & Adafruit_SSD1306 for Arduino Zero: http://forum.arduino.cc/index.php?action=dlattach;topic=378122.0;attach=154429

**Required Libraries for Arduino Mega2560**
The main microcontroller of the project, the Arduino Mega2560 requires the following libraries:
1. Arduino Timer Interupts: https://github.com/carlosrafaelgn/ArduinoTimer