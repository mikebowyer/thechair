// Do not remove the include below
#include "TheChair.h"
#include "Arduino.h"
#include "RobotEQ.h"
#include "ultrasonics.h"
#include "Timer1.h"
#include "Timer3.h"
#include "Timer4.h"
#include "Timer5.h"
#include "taskflag.h"
#include "ChairControl.h"
#include <Stream.h>

#define CHANNEL_1 1
#define SENDMOTORSPEEDS



RobotEQ controller(&Serial);// Configure Actuators and Sensors Controllers
USS myUSS(A1,A0,A2,A3,2);//Start up Ultrasonic firing
int led_state;//create LED state Variable
ChairControl theChair(2);
unsigned int bin_temperature=0;
unsigned char temp_send;

/*JOYSTICK RELATED VARIABLES*/
unsigned char recd_char,payload,car_return;


TaskFlag systemStatusTask("Task1");
TaskFlag calcMotorSpeedsTask("calcMotorSpeedsTask");
TaskFlag updateUSSTask("updateUSSTask");
TaskFlag commandHMITask("commandHMITask");

void debug_USS()
{
	Serial.print("FL=");
		Serial.print(myUSS.FL_dist);
		Serial.print(" mm");
		Serial.print("\tFR=");
		Serial.print(myUSS.FR_dist);
		Serial.print(" mm");
		Serial.print("\tRL=");
		Serial.print(myUSS.RL_dist);
		Serial.print(" mm");
		Serial.print("\tRR=");
		Serial.print(myUSS.RR_dist);
		Serial.println(" mm");
}

void toggle_led()
{
	  if(led_state == 1)
	    {
	        digitalWrite(13, LOW);
	        led_state =0;
	    }else{
	        digitalWrite(13, HIGH);
	        led_state =1;
	    }
}

void sendMotorSpeeds(int lSpeed, int rSpeed)
{
	 char l_buffer[20];// the buffer is filled with the data which will be
	 char r_buffer[20];// the buffer is filled with the data which will be

	  sprintf(l_buffer, "!G %02d %d\r", 1, lSpeed);
	  sprintf(r_buffer, "!G %02d %d\r", 2, rSpeed);
#ifdef SENDMOTORSPEEDS
	  Serial.write(l_buffer);
	  Serial.write(r_buffer);
#endif
}


void setup(void)
{
	/*Setup HeartBeat LED*/
	led_state=1;
	pinMode(13, OUTPUT);
	digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)


	/*Initialize Roboteq port and wait for port to open*/
	Serial.begin(115200);
	while (!Serial) {};

	/*Initalize Xbee serial port*/
	Serial1.begin(115200);
	while (!Serial1) {};

	/*SETUP TIMER INTERUPTS*/
	startTimer1(1000000L);//System Status Update Task every 5 s
	startTimer3(50000L);//CAlculate new motors speeds task, 50 ms
	startTimer4(150000L);//Get Latest Ultrasonic Distances Task, 150ms
	startTimer5(1000000L);//Update HMI Current Enabled Function, 1s
}

void loop(void)
{

	 if(Serial1.available() > 2 ) // if serial data is available to read
	 {
		recd_char = Serial1.peek(); //read data & store it in �recd_dat�
		//Serial.println(recd_char,BIN);
		if(recd_char == 'S')
		{
			recd_char = Serial1.read();//Consume S
			payload = Serial1.read();//put status in payload
			car_return= Serial1.read();
/*
			Serial.print("\nS: ");
			Serial.print(recd_char, BIN);//print status
			Serial.print("\nPayload: ");
			Serial.print(payload, BIN);//print status
			Serial.print("\nCR: ");
			Serial.print(car_return, BIN);//print status
			Serial.print("\nCR should be: ");
			Serial.println('\r',BIN);
*/
			if( car_return == '\r'){ //data is valid
				theChair.updateStatus(payload);
/*				Serial.print("JoyEnable: ");
				Serial.println(theChair.Joy_Enable);
				Serial.print("DeadManSW: ");
				Serial.println(theChair.DeadManSW);
				Serial.print("USSEnable: ");
				Serial.println(theChair.USSPreventEnable);
				Serial.print("CruiseEnable: ");
				Serial.println(theChair.CruiseEnable);
				Serial.print("speed: ");
				Serial.println(theChair.set_speed);
				Serial.print("S: ");
				Serial.println(recd_char, BIN);//print status
*/
			}
		}else if (recd_char == 'X'){
			recd_char = Serial1.read();//Consume S
			payload = Serial1.read();//put status in payload
			car_return= Serial1.read();
			if( car_return == '\r') //data is valid
			{ //data is valid
				theChair.setJoyStickPos(HORZ_AXIS,payload);
/*
				Serial.println("X: ");
				Serial.println(theChair.joyX);
				Serial.println(recd_char,BIN);//print status
 */
			}

		}else if(recd_char=='Y'){
			recd_char = Serial1.read();//Consume S
			payload = Serial1.read();//put status in payload
			car_return= Serial1.read();
			if( car_return == '\r') //data is valid
			{ //data is valid
				theChair.setJoyStickPos(VERT_AXIS,payload);
/*
				Serial.println("Y: ");
				Serial.println(theChair.joyY);
				Serial.println(recd_char,BIN);//print status
*/
			}
		}else{
			recd_char = Serial1.read();//consume if none of the above
		}
	 }

	 /*CALCULATE AND UPDATE MOTOR SPEEDS TASK*/
    if(calcMotorSpeedsTask.check_flag()){

		calcMotorSpeedsTask.grab_taskflag();//grab task flag

		if ((millis() - theChair.getLastStatusTime() )> CONTROL_TIMEOUT )
			{
				sendMotorSpeeds(0,0);
				//Serial.println("Status Timeout! Stopping motors!");
			}else{
				theChair.updateMotorSpeeds();
				sendMotorSpeeds(theChair.getLeftSpeed(),theChair.getRightSpeed());
/*
				Serial.print("LeftSpeed = ");
				Serial.println(theChair.getLeftSpeed(),DEC);
				Serial.print("RightSpeed = ");
				Serial.println(theChair.getRightSpeed(),DEC);
*/
			}
		calcMotorSpeedsTask.reset_taskflag();// let go of task flag
	}
    /*UPDATE USS DISTANCES TASK*/
	if(updateUSSTask.check_flag()){
		updateUSSTask.grab_taskflag();
			myUSS.getUSSDistances();
			theChair.updateUSSDistances(myUSS.FL_dist,myUSS.FR_dist, myUSS.RL_dist, myUSS.RR_dist);
			//debug_USS();
		updateUSSTask.reset_taskflag();
	}
	/*UPDATE HMI TASK*/
	if(commandHMITask.check_flag()){
		commandHMITask.grab_taskflag();

		Serial1.write(0xff);
		commandHMITask.reset_taskflag();
	}
	/*UPDATE STATUS TASK*/
	if(systemStatusTask.check_flag())
	{
		systemStatusTask.grab_taskflag();

		bin_temperature = analogRead(A5);
		temp_send = bin_temperature >> 2;
		Serial1.write(temp_send);
		toggle_led();
		systemStatusTask.reset_taskflag();
	}
}

/*Update system status task flag set*/
ISR(timer1Event)// only increments a counter and once incremented 5 times, the nsets task flag
{
	resetTimer1();
	systemStatusTask.taskflag_counter++;
	if(systemStatusTask.taskflag_counter > 4)
	{
		systemStatusTask.taskflag_counter=0;
		if(!systemStatusTask.check_taskflag_in_use()){
			systemStatusTask.set_taskflag();
		}
	}
}
/*Calculate and Send new motor speeds task*/
ISR(timer3Event)
{
	resetTimer3();
	if(!calcMotorSpeedsTask.check_taskflag_in_use()){
		calcMotorSpeedsTask.set_taskflag();
	}
}

/*Grab latest Ultrasonic Distances task*/
ISR(timer4Event)
{
	resetTimer4();
	if(!updateUSSTask.check_taskflag_in_use()){
		updateUSSTask.set_taskflag();
	}
}

/* Update HMI Current Funcitons Task*/
ISR(timer5Event)
{
	resetTimer5();
	if(!commandHMITask.check_taskflag_in_use()){
		commandHMITask.set_taskflag();
	}

}

